﻿using DC;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TheMainProgram
{
    class Program
    {

        static void Main(string[] args)
        {

            #region Connecting to Blob
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("dataphile-dc");
            container.CreateIfNotExists();
            // lire le fichier du blob
            string FileName = "dc150107.all.JIT";
            //dc141231 dc150105....dc150116
            //CloudBlockBlob blockBlob = container.GetBlockBlobReference(FileName);
              CloudBlockBlob blockBlob = container.GetBlockBlobReference(FileName);



            /*
            MemoryStream ms;
            using ( ms = new MemoryStream())
           
            using (FileStream fileg = new FileStream(@"C:\Users\syanes\Desktop\files\dc150101.all.JIT", FileMode.Open, FileAccess.Read))
            {
               
                byte[] bytes = new byte[fileg.Length];
                fileg.Read(bytes, 0, (int)fileg.Length);
                ms.Write(bytes, 0, (int)fileg.Length);
            }
            */
            
            MemoryStream memoryStream;
            using (memoryStream = new MemoryStream())

            { blockBlob.DownloadToStream(memoryStream); }
            IFidelityFile file = new FidelityMemoryStream(memoryStream);
            //IFidelityFile file = new FidelityMemoryStream(ms);
            #endregion
            #region Inserting DC data into DB

            DCtoDB DCtoDB = new DCtoDB();
            DCtoDB.SendDCtoDB(file, FileName);

            #endregion

        }
        
    }

}




