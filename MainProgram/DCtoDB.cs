﻿using DC;
using DC.Query;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;

namespace TheMainProgram
{
    public interface IDCtoDB
    {
        void SendDCtoDB(IFidelityFile file, string FileName);
    }
    public class DCtoDB : IDCtoDB
    {
        public void SendDCtoDB(IFidelityFile file, string FileName)
        {





            List<HUB_CLIENT> ListClientHub = new List<HUB_CLIENT>();
            List<HUB_PORTFOLIO> ListPortfolioHub = new List<HUB_PORTFOLIO>();
            List<SAT_CLIENT> ListClientSat = new List<SAT_CLIENT>();
            Database.SetInitializer<data_vault_dev_db_01Entities4>(new CreateDatabaseIfNotExists<data_vault_dev_db_01Entities4>());
            using (var ctx = new data_vault_dev_db_01Entities4())
            {

                List<string> filePfCodeHubs = new List<string>();
                List<string> fileClCodeHubs = new List<string>();


                SAT_CLIENT clientsat;
                SAT_PORTFOLIO portfoliosat_monthly;
                SAT_PORTFOLIO portfoliosat;
                SAT_PORTFOLIO portfoliosat_daily;






                foreach (FidelityFileLine line in file.List())
                {






                    #region Preparing the RECORD

                    IClientSat __ClientSat = new ClientSat(line);
                    IClientHub __ClientHub = new ClientHub();
                    IPortfolioSat __PortfolioSat = new PortfolioSat(line);
                    IMonthlyPortfolioSat __MonthlyPortfolioSat = new MonthlyPortfolioSat(line);
                    IDailyPortfolioSat __DailyPortfolioSat = new DailyPortfolioSat(line);
                    IPortfolioHub __PortfolioHub = new PortfolioHub();

                    IConvertToJSon __ConvertToJson = new ConvertToJson();
                    string __JsonClientSat = __ConvertToJson.ConvertingToJson(__ClientSat);
                    DateTime Record_date_clientsat = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));

                    string __JsonPortfolioSat = __ConvertToJson.ConvertingToJson(__PortfolioSat);
                    DateTime Record_date_standard = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));

                    string __JsonMonthlyPortfolioSat = __ConvertToJson.ConvertingToJson(__MonthlyPortfolioSat);
                    DateTime Record_date_monthly = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));

                    string __JsonDailyPortfolioSat = __ConvertToJson.ConvertingToJson(__DailyPortfolioSat);
                    DateTime Record_date_daily = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));

                    #endregion

                    #region Insert  all the Clients Hub and SAT from this file to a list

                    HUB_CLIENT clienthub = new HUB_CLIENT();
                    clienthub.CODE = __ClientHub.getAccountRoot(line);
                    clienthub.LOAD_DATE = __ClientHub.getLoadDate();
                    clienthub.RECORD_SRC = __ClientHub.getRecordSrc();

                    var listOfCoherentClientSat = new List<SAT_CLIENT>();
                    bool test = true;
                    clientsat = new SAT_CLIENT();
                    clientsat.RECORD = __JsonClientSat;
                    clientsat.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss"), "yyyyMMdd hh:mm:ss", CultureInfo.InvariantCulture);
                    clientsat.SATELLITE_TYPE = "CLIENT";
                    clientsat.RECORD_DATE = Record_date_clientsat;
                    //// modifié
                    clientsat.RECORD_SRC = "DC";


                    bool New_Hub = !fileClCodeHubs.Contains(clienthub.CODE);
                    if (New_Hub)
                    {
                        fileClCodeHubs.Add(clienthub.CODE);

                        clienthub.SAT_CLIENT.Add(clientsat);
                        ListClientHub.Add(clienthub);
                    }
                    else
                    {
                        var hub = new HUB_CLIENT();
                        hub = ListClientHub.Where(a => a.CODE == clienthub.CODE).First();
                        listOfCoherentClientSat = hub.SAT_CLIENT.ToList();
                        foreach (var sat in listOfCoherentClientSat)
                        {
                            if (sat.RECORD == clientsat.RECORD)
                            {
                                test = false;
                                break;
                            }
                        }
                        if (test)
                        {

                            hub.SAT_CLIENT.Add(clientsat);
                            //Console.WriteLine(clienthub.CODE);
                            //if (clienthub.CODE == "3JXW0FF")
                            //{
                            //    Console.WriteLine(clientsat.RECORD);
                            //}
                        }

                    }


                    #endregion


                    #region Insert all the Portfolios Hub and SAT from this file to a list


                    HUB_PORTFOLIO portfoliohub = new HUB_PORTFOLIO();
                    portfoliohub.LOAD_DATE = __PortfolioHub.getLoadDate();
                    portfoliohub.RECORD_SRC = __PortfolioHub.getRecordSrc();
                    portfoliohub.CODE = __PortfolioHub.getAccountNo(line);

                    portfoliosat_monthly = new SAT_PORTFOLIO();
                    portfoliosat = new SAT_PORTFOLIO();

                    portfoliosat_daily = new SAT_PORTFOLIO();

                    portfoliosat_daily.RECORD = __JsonDailyPortfolioSat;
                    portfoliosat_daily.RECORD_DATE = Record_date_daily;
                    portfoliosat_daily.SATELLITE_TYPE = "DAILY";
                    portfoliosat_daily.RECORD_SRC = "DC";
                    portfoliosat_daily.LOAD_DATE = portfoliohub.LOAD_DATE;

                    portfoliosat_monthly.RECORD = __JsonMonthlyPortfolioSat;
                    portfoliosat_monthly.RECORD_DATE = Record_date_monthly;
                    portfoliosat_monthly.SATELLITE_TYPE = "MONTHLY";
                    portfoliosat_monthly.RECORD_SRC = "DC";
                    portfoliosat_monthly.LOAD_DATE = portfoliohub.LOAD_DATE;

                    portfoliosat.RECORD = __JsonPortfolioSat;
                    portfoliosat.RECORD_DATE = Record_date_standard;
                    portfoliosat.SATELLITE_TYPE = "STANDARD";
                    portfoliosat.RECORD_SRC = "DC";
                    portfoliosat.LOAD_DATE = portfoliohub.LOAD_DATE;

                    bool New_Hub2 = !filePfCodeHubs.Contains(portfoliohub.CODE);
                    if (New_Hub2)
                    {
                        filePfCodeHubs.Add(portfoliohub.CODE);
                        portfoliohub.SAT_PORTFOLIO.Add(portfoliosat_daily);
                        portfoliohub.SAT_PORTFOLIO.Add(portfoliosat_monthly);
                        portfoliohub.SAT_PORTFOLIO.Add(portfoliosat);
                        ListPortfolioHub.Add(portfoliohub);
                    }


                    #endregion


                }


                //extract existing hubs from the DB

                List<HUB_CLIENT> db_HUBS_CL = ctx.HUB_CLIENT.Where(hub => fileClCodeHubs.Contains(hub.CODE)).ToList();
                List<HUB_PORTFOLIO> db_HUBS_PF = ctx.HUB_PORTFOLIO.Where(hub => filePfCodeHubs.Contains(hub.CODE)).ToList();
                var addClientHubList = new List<HUB_CLIENT>();
                var addPortfolioHubList = new List<HUB_PORTFOLIO>();
                long verificationSQN;
                //extract only new client hubs

                var hubToVerifyClientSatSQNs = new List<long>();

                var dbClientCode = db_HUBS_CL.Select(m => m.CODE).Distinct().ToList();
                foreach (var item in fileClCodeHubs)
                {



                    if (!dbClientCode.Contains(item))
                    {
                        //extract only new hubs
                        var h = new HUB_CLIENT();
                        h = ListClientHub.Where(hub => hub.CODE == item).First();
                        var s = new SAT_CLIENT();
                        s = h.SAT_CLIENT.ToList()[0];
                        h.SAT_CLIENT.Clear();
                        h.SAT_CLIENT.Add(s);
                        addClientHubList.Add(h);
                    }
                    else
                    {
                        //extract SQN of non new huhs .so we can compare the sats 
                        verificationSQN = db_HUBS_CL.Where(hub => hub.CODE == item).FirstOrDefault().SQN;
                        if (!hubToVerifyClientSatSQNs.Contains(verificationSQN))
                        {
                            hubToVerifyClientSatSQNs.Add(verificationSQN);
                        }

                    }
                }

                ctx.Set<HUB_CLIENT>().AddRange(addClientHubList);

                //extract only new portfolio hubs

                var hubToVerifyPortfilioSatSQNs = new List<long>();
                var dbPortfolioCode = db_HUBS_PF.Select(m => m.CODE).Distinct().ToList();
                foreach (var item in filePfCodeHubs)
                {
                    if (!dbPortfolioCode.Contains(item))
                    {
                        //extract only new hubs
                        addPortfolioHubList.Add(ListPortfolioHub.Where(hub => hub.CODE == item).FirstOrDefault());
                    }
                    else
                    {
                        //extract SQN of non new huhs. So we can compare the sats
                        verificationSQN = db_HUBS_PF.Where(hub => hub.CODE == item).FirstOrDefault().SQN;
                        if (!hubToVerifyPortfilioSatSQNs.Contains(verificationSQN))
                        {
                            hubToVerifyPortfilioSatSQNs.Add(verificationSQN);
                        }


                    }
                }


                ctx.Set<HUB_PORTFOLIO>().AddRange(addPortfolioHubList);

                //Console.WriteLine("addClientHubList.Count = " + addClientHubList.Count);
                //Console.WriteLine("addPortfolioHubList.Count = " + addPortfolioHubList.Count);

                //Console.WriteLine("hubToVerifyClientSatSQNs != null " + hubToVerifyClientSatSQNs != null);
                //Console.WriteLine(hubToVerifyClientSatSQNs.Count > 0);
                //Console.WriteLine("hubToVerifyClientSatSQNs.Count = " + hubToVerifyClientSatSQNs.Count);

                if (hubToVerifyClientSatSQNs.Count > 0)
                {



                    //extract existing sats
                    //var SATSClientUnique = new List<SAT_CLIENT>();
                    List<SAT_CLIENT> db_SATS_CL = ctx.SAT_CLIENT.Where(sat => hubToVerifyClientSatSQNs.Contains(sat.SQN)).GroupBy(s => s.SQN).Select(s => s.OrderByDescending(sat => sat.RECORD_DATE).FirstOrDefault()).ToList();
                    //db_SATS_CL = ctx.SAT_CLIENT.Where(sat => hubToVerifyClientSatSQNs.Contains(sat.SQN)).OrderByDescending(sat => sat.RECORD_DATE).GroupBy(s => s.SQN).Select(s => s).ToList();
                    /*foreach (var item in hubToVerifyClientSatSQNs)
                    {
                        SATSClientUnique.Add(db_SATS_CL.Where(s => s.SQN == item).OrderByDescending(sat => sat.RECORD_DATE).First());
                    }*/

                    //compare sats
                    //Console.WriteLine("db_SATS_CL = " + db_SATS_CL.Count);
                    //Console.WriteLine("SATSClientUnique.Count = " + SATSClientUnique.Count);
                    //Console.WriteLine("ListClientHub = " + ListClientHub.Count);


                    var addClientSatList = new List<SAT_CLIENT>();
                    string x;
                    //int t = 0;
                    //foreach (var item in db_SATS_CL)
                    //{
                    //    x = db_HUBS_CL.Where(h => h.SQN == item.SQN).First().CODE;
                    //    var clsat = ListClientSat.Where(s => s.RECORD_SRC == x).FirstOrDefault();
                    //    if (clsat.RECORD != item.RECORD)
                    //    {
                    //        t++;
                    //    }
                    //}
                    //Console.WriteLine("t ============" +t);

                    //int i = 0;
                    foreach (var item in db_SATS_CL)
                    {
                        var toCompare = new List<SAT_CLIENT>();
                        x = db_HUBS_CL.Where(h => h.SQN == item.SQN).First().CODE;

                        toCompare = ListClientHub.Where(l => l.CODE == x).First().SAT_CLIENT.ToList();
                        foreach (var n in toCompare)
                        {


                            if (item.RECORD_DATE < n.RECORD_DATE && item.RECORD != n.RECORD)
                            {
                                //i++;

                                //add the chosen sats to the list of addition
                                n.SQN = item.SQN;
                                addClientSatList.Add(n);
                                break;
                            }

                        }

                    }
                    //Console.WriteLine("i=========== "+i);
                    //Console.WriteLine("addClientSatList.Count = " + addClientSatList.Count);
                    ctx.Set<SAT_CLIENT>().AddRange(addClientSatList);

                }


                //Console.WriteLine("hubToVerifyPortfilioSaQNs!=null" + hubToVerifyPortfilioSatSQNs != null);
                //Console.WriteLine(hubToVerifyPortfilioSatSQNs.Count > 0);
                //Console.WriteLine("hubToVerifyPortfilioSatSQNs.Count = " + hubToVerifyPortfilioSatSQNs.Count);

                if (hubToVerifyPortfilioSatSQNs.Count > 0)
                {



                    //extract existing sats
                    var SATSPortfolioUnique = ctx.SAT_PORTFOLIO.Where(sat => hubToVerifyPortfilioSatSQNs.Contains(sat.SQN) && sat.SATELLITE_TYPE == "STANDARD").GroupBy(s => s.SQN).Select(s => s.OrderByDescending(sat => sat.RECORD_DATE).FirstOrDefault()).ToList();

                    var SATSPortfolioDailyUnique = ctx.SAT_PORTFOLIO.Where(sat => hubToVerifyPortfilioSatSQNs.Contains(sat.SQN) && sat.SATELLITE_TYPE == "DAILY").GroupBy(s => s.SQN).Select(s => s.OrderByDescending(sat => sat.RECORD_DATE).FirstOrDefault()).ToList();

                    var SATSPortfolioMonthlyUnique = ctx.SAT_PORTFOLIO.Where(sat => hubToVerifyPortfilioSatSQNs.Contains(sat.SQN) && sat.SATELLITE_TYPE == "MONTHLY").GroupBy(s => s.SQN).Select(s => s.OrderByDescending(sat => sat.RECORD_DATE).FirstOrDefault()).ToList();

                    /*List<SAT_PORTFOLIO> db_SATS_PF = ctx.SAT_PORTFOLIO.Where(sat => hubToVerifyPortfilioSatSQNs.Contains(sat.SQN)&& sat.SATELLITE_TYPE == "STANDARD").GroupBy(s => s.SQN).Select(s => s.OrderByDescending(sat => sat.RECORD_DATE).FirstOrDefault()).ToList();
                    foreach (var item in hubToVerifyPortfilioSatSQNs)
                    {
                        SATSPortfolioUnique.Add(db_SATS_PF.Where(s => s.SQN == item && s.SATELLITE_TYPE == "STANDARD").OrderByDescending(sat => sat.LOAD_DATE).First());
                        SATSPortfolioDailyUnique.Add(db_SATS_PF.Where(s => s.SQN == item && s.SATELLITE_TYPE == "DAILY").OrderByDescending(sat => sat.LOAD_DATE).First());
                        SATSPortfolioMonthlyUnique.Add(db_SATS_PF.Where(s => s.SQN == item && s.SATELLITE_TYPE == "MONTHLY").OrderByDescending(sat => sat.LOAD_DATE).First());
                    }*/
                    //compare sats
                    //Console.WriteLine("db_SATS_PF.Count = " + db_SATS_PF.Count);
                    //Console.WriteLine("SATSPortfolioUnique.Count = " + SATSPortfolioUnique.Count);
                    //Console.WriteLine("SATSPortfolioDailyUnique.Count = " + SATSPortfolioDailyUnique.Count);
                    //Console.WriteLine("SATSPortfolioMonthlyUnique.Count = " + SATSPortfolioMonthlyUnique.Count);

                    var addPorfolioSatList = new List<SAT_PORTFOLIO>();
                    var addPorfolioSatDailyList = new List<SAT_PORTFOLIO>();
                    var addPorfolioSatMounthlyList = new List<SAT_PORTFOLIO>();

                    /* if (SATSPortfolioUnique.Count == SATSPortfolioDailyUnique.Count && SATSPortfolioDailyUnique.Count == SATSPortfolioMonthlyUnique.Count)
                     {
                         var x1 = new HUB_PORTFOLIO();
                         var satList1 = new List<SAT_PORTFOLIO>();
                         SAT_PORTFOLIO toCompare1;
                         var x2 = new HUB_PORTFOLIO();
                         var satList2 = new List<SAT_PORTFOLIO>();
                         SAT_PORTFOLIO toCompare2;

                         var x3 = new HUB_PORTFOLIO();
                         var satList3 = new List<SAT_PORTFOLIO>();
                         SAT_PORTFOLIO toCompare3;

                         for (int i = 0; i < SATSPortfolioUnique.Count; i++)
                         {
                             toCompare1 = new SAT_PORTFOLIO();

                             x1 = db_HUBS_PF.Where(h => h.SQN == SATSPortfolioUnique[i].SQN).First();



                             satList1 = ListPortfolioHub.Where(l => l.CODE == x1.CODE).FirstOrDefault().SAT_PORTFOLIO.ToList();
                             toCompare1 = satList1.Where(n => n.SATELLITE_TYPE == "STANDARD").FirstOrDefault();

                             if (SATSPortfolioUnique[i].RECORD_DATE < toCompare1.RECORD_DATE && SATSPortfolioUnique[i].RECORD != toCompare1.RECORD)
                             {
                                 //add the chosen sats to the list of addition
                                 toCompare1.SQN = SATSPortfolioUnique[i].SQN;
                                 addPorfolioSatList.Add(toCompare1);
                             }


                         toCompare2 = new SAT_PORTFOLIO();
                         x2 = db_HUBS_PF.Where(h => h.SQN == SATSPortfolioDailyUnique[i].SQN).First();


                         satList2 = ListPortfolioHub.Where(l => l.CODE == x2.CODE).FirstOrDefault().SAT_PORTFOLIO.ToList();
                         toCompare2 = satList2.Where(n => n.SATELLITE_TYPE == "DAILY").FirstOrDefault();
                         if (SATSPortfolioDailyUnique[i].RECORD_DATE < toCompare2.RECORD_DATE && SATSPortfolioDailyUnique[i].RECORD != toCompare2.RECORD)
                         {
                             //add the chosen sats to the list of addition
                             toCompare2.SQN = SATSPortfolioDailyUnique[i].SQN;
                             addPorfolioSatDailyList.Add(toCompare2);
                         }

                             toCompare3 = new SAT_PORTFOLIO();
                             x3 = db_HUBS_PF.Where(h => h.SQN == SATSPortfolioMonthlyUnique[i].SQN).First();
                             //if (x != null)
                             //{


                             satList3 = ListPortfolioHub.Where(l => l.CODE == x3.CODE).FirstOrDefault().SAT_PORTFOLIO.ToList();
                             toCompare3 = satList3.Where(n => n.SATELLITE_TYPE == "MONTHLY").FirstOrDefault();

                             if (SATSPortfolioMonthlyUnique[i].RECORD_DATE < toCompare3.RECORD_DATE && SATSPortfolioMonthlyUnique[i].RECORD != toCompare3.RECORD)
                             {
                                 //add the chosen sats to the list of addition
                                 toCompare3.SQN = SATSPortfolioMonthlyUnique[i].SQN;
                                 addPorfolioSatMounthlyList.Add(toCompare3);
                             }
                         }
                     }
                     else
                     {*/
                    var x = new HUB_PORTFOLIO();
                    var satList = new List<SAT_PORTFOLIO>();
                    SAT_PORTFOLIO toCompare;
                    foreach (var item in SATSPortfolioUnique)
                    {
                        toCompare = new SAT_PORTFOLIO();

                        x = db_HUBS_PF.Where(h => h.SQN == item.SQN).First();

                        //if (x != null)
                        //{ 

                        satList = ListPortfolioHub.Where(l => l.CODE == x.CODE).FirstOrDefault().SAT_PORTFOLIO.ToList();
                        toCompare = satList.Where(n => n.SATELLITE_TYPE == "STANDARD").FirstOrDefault();

                        if (item.RECORD_DATE < toCompare.RECORD_DATE && item.RECORD != toCompare.RECORD)
                        {
                            //add the chosen sats to the list of addition
                            toCompare.SQN = item.SQN;
                            addPorfolioSatList.Add(toCompare);
                        }
                        //}

                        //else
                        //{
                        //    toCompare.SQN = item.SQN;
                        //    addPorfolioSatList.Add(toCompare);
                        //}

                    }
                    foreach (var item in SATSPortfolioDailyUnique)
                    {
                        toCompare = new SAT_PORTFOLIO();
                        x = db_HUBS_PF.Where(h => h.SQN == item.SQN).First();

                        //if (x != null)
                        //{

                        satList = ListPortfolioHub.Where(l => l.CODE == x.CODE).FirstOrDefault().SAT_PORTFOLIO.ToList();
                        toCompare = satList.Where(n => n.SATELLITE_TYPE == "DAILY").FirstOrDefault();
                        if (item.RECORD_DATE < toCompare.RECORD_DATE && item.RECORD != toCompare.RECORD)
                        {
                            //add the chosen sats to the list of addition
                            toCompare.SQN = item.SQN;
                            addPorfolioSatDailyList.Add(toCompare);
                        }
                        //}

                        //else
                        //{
                        //    toCompare.SQN = item.SQN;
                        //    addPorfolioSatList.Add(toCompare);
                        //}
                    }
                    foreach (var item in SATSPortfolioMonthlyUnique)
                    {
                        toCompare = new SAT_PORTFOLIO();
                        x = db_HUBS_PF.Where(h => h.SQN == item.SQN).First();
                        //if (x != null)
                        //{


                        satList = ListPortfolioHub.Where(l => l.CODE == x.CODE).FirstOrDefault().SAT_PORTFOLIO.ToList();
                        toCompare = satList.Where(n => n.SATELLITE_TYPE == "MONTHLY").FirstOrDefault();

                        if (item.RECORD_DATE < toCompare.RECORD_DATE && item.RECORD != toCompare.RECORD)
                        {
                            //add the chosen sats to the list of addition
                            toCompare.SQN = item.SQN;
                            addPorfolioSatMounthlyList.Add(toCompare);
                        }
                        //}

                        //else
                        //{
                        //    toCompare.SQN = item.SQN;
                        //    addPorfolioSatList.Add(toCompare);
                        //}

                    }
                    /* }*/
                    //Console.WriteLine("addPorfolioSatList.Count = " + addPorfolioSatList.Count);
                    //Console.WriteLine("addPorfolioSatDailyList.Count = " + addPorfolioSatDailyList.Count);
                    //Console.WriteLine("addPorfolioSatMounthlyList.Count = " + addPorfolioSatMounthlyList.Count);
                    ctx.Set<SAT_PORTFOLIO>().AddRange(addPorfolioSatList);
                    ctx.Set<SAT_PORTFOLIO>().AddRange(addPorfolioSatDailyList);
                    ctx.Set<SAT_PORTFOLIO>().AddRange(addPorfolioSatMounthlyList);
                }

                //Console.WriteLine(addClientHubList[0].SQN);
                //
                //


                ctx.SaveChanges();
                //Console.WriteLine("finich");
                //Console.ReadKey();
            }


        }





    }
}
