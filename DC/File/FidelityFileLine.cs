﻿using System;
using System.Globalization;

namespace DC
{
    public class FidelityFileLine : IFidelityFileLine
    {
        private string _line;
        public FidelityFileLine(string line)
        {
            _line = line;
        }

        public string CustomerCode()
        {
            return new FidelityString(_line, 1, 3).Value();
        }
        public string AccountNo()
        {
            return new FidelityString(_line, 4, 8).Value();
        }
        public string GuarantorCode()
        {
            return new FidelityString(_line, 12, 1).Value();
        }
        public string GuarantorAccount()
        {
            return new FidelityString(_line, 13, 8).Value();
        }
        public string AccountType()
        {
            return new FidelityString(_line, 21, 1).Value();
        }
        public string CurrencyCode1()
        {
            return new FidelityString(_line, 22, 3).Value();
        }
        public string IaNo()
        {
            return new FidelityString(_line, 25, 4).Value();
        }
        public string AccountClassCode()
        {
            return new FidelityString(_line, 29, 2).Value();
        }
        public string OptionCode()
        {
            return new FidelityString(_line, 31, 1).Value();
        }
        public string DebitInterestCode()
        {
            return new FidelityString(_line, 32, 4).Value();
        }
        public string CreditInterestCode()
        {
            return new FidelityString(_line, 36, 4).Value();
        }
        public string ResidenceCode()
        {
            return new FidelityString(_line, 40, 3).Value();
        }
        public string NRTCode()
        {
            return new FidelityString(_line, 43, 1).Value();
        }
        public string ShortName()
        {
            return new FidelityString(_line, 44, 16).Value();
        }
        public double MonthEndTradeDateBalance()
        {

            TrailingSignApplied b =  new TrailingSignApplied
                        (new FidelityString(_line, 60, 14));



            double __MonthEndTradeDateBalance = new DecimalsApplied
                (new TrailingSignApplied
                        (new FidelityString(_line, 60, 14))
                        , 2)
                        .Value();

            return __MonthEndTradeDateBalance;


        }
        public double MonthEndSettlementDateBalance()
        {
            double __MonthEndSettlementDateBalance = new DecimalsApplied
                (new TrailingSignApplied
                        (new FidelityString(_line, 74, 14))
                        , 2)
                        .Value();

            return __MonthEndSettlementDateBalance;
        }
        public DateTime LastActivityDate()
        {

            string strDate = new FidelityString(_line, 88, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }
        }
        public double CurrentTradeDateBalance()
        {
            double __CurrentTradeDateBalance = new DecimalsApplied
               (new TrailingSignApplied
                       (new FidelityString(_line, 96, 14))
                       , 2)
                       .Value();

            return __CurrentTradeDateBalance;
        }
        public double CurrentSettlementDateBalance()
        {
            double __CurrentSettlementDateBalance = new DecimalsApplied
               (new TrailingSignApplied
                       (new FidelityString(_line, 110, 14))
                       , 2)
                       .Value();

            return __CurrentSettlementDateBalance;
        }
        public double MarketValue()
        {
            double __MarketValue = new DecimalsApplied
                           (new TrailingSignApplied
                                   (new FidelityString(_line, 124, 14))
                                   , 2)
                                   .Value();

            return __MarketValue;
        }
        public double LoanValue()
        {
            double __LoanValue = new DecimalsApplied
               (new TrailingSignApplied
                       (new FidelityString(_line, 138, 14))
                       , 2)
                       .Value();

            return __LoanValue;
        }
        public double RTMorExcess()
        {
            double __RTMorExcess = new DecimalsApplied
               (new TrailingSignApplied
                       (new FidelityString(_line, 152, 14))
                       , 2)
                       .Value();

            return __RTMorExcess;
        }
        
        public double RTSorEquity()
        {
            double __RTSorEquity = new DecimalsApplied
              (new TrailingSignApplied
                      (new FidelityString(_line, 166, 14))
                      , 2)
                      .Value();

            return __RTSorEquity;
        }
        public double CapitalRequired()
        {
            double __CapitalRequired = new DecimalsApplied
               (new TrailingSignApplied
                       (new FidelityString(_line, 180, 14))
                       , 2)
                       .Value();

            return __CapitalRequired;
        }

        public string AccountStatusCode()
        {
            return new FidelityString(_line, 194, 1).Value();
        }
        public string ClientID()
        {
            return new FidelityString(_line, 195, 7).Value();
        }
        public string Language1()
        {
            return new FidelityString(_line, 202, 1).Value();
        }
        public string AdressLine1()
        {
            return new FidelityString(_line, 203, 35).Value();
        }
        public string AdressLine2()
        {
            return new FidelityString(_line, 238, 35).Value();
        }
        public string AdressLine3()
        {
            return new FidelityString(_line, 273, 35).Value();
        }
        public string AdressLine4()
        {
            return new FidelityString(_line, 308, 35).Value();
        }
        public string AdressLine5()
        {
            return new FidelityString(_line, 343, 35).Value();
        }
        public string AdressLine6()
        {
            return new FidelityString(_line, 378, 35).Value();
        }
        public string AdressLine7()
        {
            return new FidelityString(_line, 413, 35).Value();
        }
        public string AdressLine8()
        {
            return new FidelityString(_line, 448, 35).Value();
        }
        public string PostCode()
        {
            return new FidelityString(_line, 483, 9).Value();
        }
        public string HomePhoneNumber()
        {
            return new FidelityString(_line, 492, 20).Value();
        }
        public string WorkPhoneNumber()
        {
            return new FidelityString(_line, 512, 20).Value();
        }
        public string FaxNumber()
        {
            return new FidelityString(_line, 532, 20).Value();
        }
        public string SIN()
        {
            return new FidelityString(_line, 552, 15).Value();
        }
        public double AnnualIncome()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 567, 10)), 2).Value();
        }
        public double NetWorth()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 577, 11)), 2).Value();
        }
        public string MaritalStatus()
        {
            return new FidelityString(_line, 588, 1).Value();
        }
        public double NumberofDependants()
           
        {
            return new  FidelityDouble(new FidelityString(_line, 589, 2)).Value();
        }
        public DateTime AccountOpenDate()
        {
            string strDate = new FidelityString(_line, 591, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public string IAComment()
        {
            return new FidelityString(_line, 599, 32).Value();
        }
        public string UserComment()
        {
            return new FidelityString(_line, 631, 40).Value();
        }
        public string DeliveryCode()
        {
            return new FidelityString(_line, 671, 1).Value();
        }
        public string CreditLimit()
        {
            return new FidelityString(_line, 672, 1).Value();
        }
        public string CurrencyCode2()
        {
            return new FidelityString(_line, 673, 3).Value();
        }
        public string DivendedConfirmationNoticeCode()
        {
            return new FidelityString(_line, 676, 1).Value();
        }
        public string AccountSubType1()
        {
            return new FidelityString(_line, 677, 1).Value();
        }
        public string ClientLastName()
        {
            return new FidelityString(_line, 678, 20).Value();
        }
        public string ClientFirstName()
        {
            return new FidelityString(_line, 698, 15).Value();
        }
        public double HouseholdID()
        {
            return new FidelityDouble(new FidelityString(_line, 713, 7)).Value();

        }
        public string HouseholdName()
        {
            return new FidelityString(_line, 720, 30).Value();
        }
        public string Objecting()
        {
            return new FidelityString(_line, 750, 1).Value();
        }
        public string Mail()
        {
            return new FidelityString(_line, 751, 1).Value();
        }
        public string Email()
        {
            return new FidelityString(_line, 752, 1).Value();
        }
        public string Language2()
        {
            return new FidelityString(_line, 753, 1).Value();
        }
        public DateTime AccountCloseDate()
        {

            string strDate = new FidelityString(_line, 754, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }
        }
        
        public double MarketValueforShortPositions()
        {
            double __MarketValueforShortPositions = new DecimalsApplied
              (new TrailingSignApplied
                      (new FidelityString(_line, 762, 14))
                      , 4)
                      .Value();

            return __MarketValueforShortPositions;
        }
        public string ElectronicConfirmsFlag()
        {
            return new FidelityString(_line, 776, 1).Value();
        }
        public string ElectronicStatementsFlag()
        {
            return new FidelityString(_line, 777, 1).Value();
        }
        public DateTime BirthDate()
        {
            string strDate = new FidelityString(_line, 778, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

            //ddate = new FidelityString(_line, 778, 8).Value();
            //ddate = ddate.Replace(" ", "");
            //if (ddate == "")

            //    return DateTime.ParseExact("00991231", "yyyyMMdd", CultureInfo.InvariantCulture);

            //else
            //    return DateTime.ParseExact(new FidelityString(_line, 778, 8).Value(), "yyyyMMdd", CultureInfo.InvariantCulture);


        }
        public string FirstEmail()
        {
            return new FidelityString(_line, 786, 40).Value();
        }
        public string SecondEmail()
        {
            return new FidelityString(_line, 826, 40).Value();
        }
        public string ThirdEmail()
        {
            return new FidelityString(_line,866, 40).Value();
        }
        public string FourthEmail()
        {
            return new FidelityString(_line, 906, 40).Value();
        }
        public string Gender()
        {
            return new FidelityString(_line, 946, 1).Value();
        }
        public string PortfolioFlag()
        {
            return new FidelityString(_line, 947, 2).Value();
        }
        public double AccountAccruedInterest1()
        {
            double __AccountAccruedInterest = new DecimalsApplied
             (new TrailingSignApplied
                     (new FidelityString(_line, 949, 14))
                     , 4)
                     .Value();

            return __AccountAccruedInterest;
        }

        public DateTime LastTradeDate1()
        {

            string strDate = new FidelityString(_line, 963, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }
        }
        public string AccountSubType2()
        {
            return new FidelityString(_line, 971, 1).Value();
        }
        public DateTime LastUpdateDateofClientRecord()
        {

            string strDate = new FidelityString(_line, 972, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }
        }
        public DateTime LastUpdateDateofAccountRecord()
        {

            string strDate = new FidelityString(_line, 980, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }
        }
        public DateTime LastUpdateDateofAddressRecord()
        {

            string strDate = new FidelityString(_line, 988, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }
        }
        public string TradeOk()
        {
            return new FidelityString(_line, 996, 1).Value();
        }
        public string SpouseName()
        {
            return new FidelityString(_line, 997, 20).Value();
        }
        public double SpouseSIN()
        {
            return new FidelityDouble(new FidelityString(_line, 1017, 9)).Value();
        }
        public double ClientRecipientType()
        {
            return new FidelityDouble(new FidelityString(_line, 1026, 1)).Value();
        }
        public string LastHousehold()
        {
            return new FidelityString(_line, 1027, 8).Value();
        }
        public string PortfolioType()
        {
            return new FidelityString(_line, 1035, 4).Value();
        }

        public double AccountAccruedInterest2()
        {
            double __AccountAccruedInterest = new DecimalsApplied
             (new TrailingSignApplied
                     (new FidelityString(_line, 1039, 14))
                     , 4)
                     .Value();

            return __AccountAccruedInterest;
        }
        public DateTime LastTradeDate2()
        {
            string strDate = new FidelityString(_line, 1053, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }
        }

        public DateTime SpousalBirthDate()
        {

            string strDate = new FidelityString(_line, 1061, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }
        }
        public string AccountITF()
        {
            return new FidelityString(_line, 1069, 3).Value();
        }
        public string Objective1()
        {
            return new FidelityString(_line, 1072, 5).Value();
        }
        public string Objective2()
        {
            return new FidelityString(_line, 1077, 5).Value();
        }
        public string Objective3()
        {
            return new FidelityString(_line, 1082, 5).Value();
        }
        public string Objective4()
        {
            return new FidelityString(_line, 1087, 5).Value();
        }
        public string Objective5()
        {
            return new FidelityString(_line, 1092, 5).Value();
        }
        public string Objective6()
        {
            return new FidelityString(_line, 1097, 5).Value();
        }
        public string Objective7()
        {
            return new FidelityString(_line, 1102, 5).Value();
        }
        public string Objective8()
        {
            return new FidelityString(_line, 1107, 5).Value();
        }
        public string Risk1()
        {
            return new FidelityString(_line, 1112, 5).Value();
        }
        public string Risk2()
        {
            return new FidelityString(_line, 1117, 5).Value();
        }
        public string Risk3()
        {
            return new FidelityString(_line, 1122, 5).Value();
        }
        public string Risk4()
        {
            return new FidelityString(_line, 1127, 5).Value();
        }
        public string Risk5()
        {
            return new FidelityString(_line, 1132, 5).Value();
        }
        public string Risk6()
        {
            return new FidelityString(_line, 1137, 5).Value();
        }
        public string Risk7()
        {
            return new FidelityString(_line, 1142, 5).Value();
        }
        public string Risk8()
        {
            return new FidelityString(_line, 1147, 5).Value();
        }
        public double TimeHorizonYear()
        {
            return new FidelityDouble(new FidelityString(_line, 1152, 4)).Value();
        }
        public double TimeHorizonCode()
        {
            return new FidelityDouble(new FidelityString(_line, 1156, 2)).Value();
        }
        public double ClientNetWorth()
        {
            return new FidelityDouble(new FidelityString(_line, 1158, 15)).Value();
        }
        public string Occupation()
        {
            return new FidelityString(_line, 1173, 15).Value();
        }
        public string EmployerName()
        {
            return new FidelityString(_line, 1188, 30).Value();
        }
    }

















       

    }

