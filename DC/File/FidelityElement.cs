﻿using System;

namespace DC
{
    public interface IFidelityElement<T>
    {
        T Value();
    }


    public class FidelityString : IFidelityElement<string>
    {
        string _lineString;
        int _offset;
        int _length;
        bool _trailingSign;

        public FidelityString(string lineString, int offset, int length) : this(lineString, offset, length, false)
        {
        }

        public FidelityString(string lineString, int offset, int length, bool trailingSign)
        {
            _lineString = lineString;
            _offset = offset;
            _length = length;
            _trailingSign = trailingSign;
        }

        public string Value()
        {
           


                if (_trailingSign)
                {
                    return _lineString.Substring(_offset - 1, _length - 1);
                }
                else
                {
                //traiter le cas ou le length du substring dépasse la longueur de la ligne du fichier

                if (_offset < _lineString.Length)
                {
                    if (_offset - 1 + _length > _lineString.Length)
                    { return _lineString.Substring(_offset - 1, _lineString.Length + 1 - _offset); }
                    else
                    { return _lineString.Substring(_offset - 1, _length); }
                }
                else
                {
                    return "";                }
                }
        }
    }

    public class FidelityDouble : IFidelityElement<double>
    {
        IFidelityElement<string> _origin;

        public FidelityDouble(IFidelityElement<string> origin)
        {
            _origin = origin;
        }

        public double Value()
        {
            if (_origin.Value().Replace(" ","") != "")
            {
                return Convert.ToDouble(_origin.Value());
            }
            else
            {
                return 0;
            }
        }
    }


    public class TrailingSignApplied : IFidelityElement<double>
    {
        IFidelityElement<string> _origin;

        public TrailingSignApplied(IFidelityElement<string> origin)
        {
            _origin = origin;
        }

        public double Value()
        {
            string substring = _origin.Value();


            if (substring != "")
            {
                if (substring[substring.Length - 1] == '-')
                {
                    substring = substring.Substring(0, _origin.Value().Length - 1);
                    return (Convert.ToDouble(substring) * -1);
                }
                else
                {
                    int success;
                    if (int.TryParse(substring, out success))
                    {
                        substring = substring.Substring(0, _origin.Value().Length - 1);
                        return (Convert.ToDouble(substring));
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else
            {
                return 0;
            }
        }
    }

    public class DecimalsApplied : IFidelityElement<double>
    {

        IFidelityElement<double> _origin;
        int _decimals;

        public DecimalsApplied(IFidelityElement<double> origin, int decimals)
        {
            _origin = origin;
            _decimals = decimals;
        }

        public double Value()
        {
            return _origin.Value() / Math.Pow(10, _decimals);

        }
    }


}
