﻿using System;
using System.Globalization;

namespace DC.Query
{
    public class DailyPortfolioSat : IDailyPortfolioSat
    {
        public DateTime LastActivityDate;
        public double CurrentTradeDateBalance;
        public double CurrentSettlementDateBalance;
        public double MarketValue;
        public double LoanValue;
        public double RTMorExcess;
        public double RTSorEquity;
        public double CapitalRequired;
        public static string satellite_type = "daily";

        public DateTime LoadDate;
        public string RecordSrc;
        public DailyPortfolioSat(FidelityFileLine line)
        {
            LastActivityDate = line.LastActivityDate();
            CurrentTradeDateBalance = line.CurrentTradeDateBalance();
            CurrentSettlementDateBalance = line.CurrentSettlementDateBalance();
            MarketValue = line.MarketValue();
            LoanValue = line.LoanValue();
            RTMorExcess = line.RTMorExcess();
            RTSorEquity = line.RTSorEquity();
            CapitalRequired = line.CapitalRequired();
            LoadDate = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd"), "yyyyMMdd", CultureInfo.InvariantCulture);
            RecordSrc = "DC";
        }
    }
}
