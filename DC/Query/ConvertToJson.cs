﻿using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace DC.Query
{
    public class ConvertToJson : IConvertToJSon
    {
        public string ConvertingToJson(object obj)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();

            settings.NullValueHandling = NullValueHandling.Ignore;
            string Json = JsonConvert.SerializeObject(obj, settings);
            return Json;

            
        }
    }
}
